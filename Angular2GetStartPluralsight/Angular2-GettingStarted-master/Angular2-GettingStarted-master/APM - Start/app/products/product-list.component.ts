import { Component, OnInit } from '@angular/core';

import { IProduct } from './product';
import { ProductService } from "./product.service";

@Component({
    // selector: 'pm-products',
    templateUrl : 'app/products/product-list.component.html',
    styleUrls: ['app/products/product-list.component.css']
})

export class ProductListComponent implements OnInit {

    
    pageTitle : string = 'Product List';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean  = false;
    listFilter: string;
    products : IProduct[] = [];
    errorMessage : string;

    constructor(private producService: ProductService){
    }

    toggleImage(): void{
        this.showImage = !this.showImage;
    }

    ngOnInit() : void{
        this.producService.getProducts()
            .subscribe(products => this.products = products,
            error => this.errorMessage = <any>error);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Produc List: ' + message;
    }
}